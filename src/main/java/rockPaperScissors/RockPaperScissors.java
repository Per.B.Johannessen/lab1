package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
     	/*
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        
        while (true) {
            
            Random r = new Random();

            int randomitem = r.nextInt(3);
            String computerMove =rpsChoices.get(randomitem);

            System.out.println("Let's play round " + roundCounter);
            System.out.printf("Your choice (Rock/Paper/Scissors)?\n");
            String playerMove = sc.nextLine();
            
            
        
            if (playerMove.equals(computerMove)){
                System.out.printf("Human chose %s, computer chose %s. It's a tie!", playerMove, computerMove);
                roundCounter ++;
            }
            else if (playerMove.equals("rock")){
                if (computerMove.equals("paper")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!", playerMove, computerMove);
                    computerScore ++;
                    roundCounter ++;
                }   
                else if (computerMove.equals("scissors")){
                System.out.printf("Human chose %s, computer chose %s. Human wins!", playerMove, computerMove);
                humanScore ++;
                roundCounter ++;
                }
            }
            else if (playerMove.equals("paper")){
                if (computerMove.equals("scissors")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!", playerMove, computerMove);
                    computerScore ++;
                    roundCounter = roundCounter + 1;
                }   
                else if (computerMove.equals("rock")){
                System.out.printf("Human chose %s, computer chose %s. Human wins!", playerMove, computerMove);
                humanScore ++;
                roundCounter ++;
                }
            }
            else if (playerMove.equals("scissors")){
                if (computerMove.equals("rock")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!", playerMove, computerMove);
                    computerScore ++;
                    roundCounter ++;
                }   
                else if (computerMove.equals("paper")){
                System.out.printf("Human chose %s, computer chose %s. Human wins!", playerMove, computerMove);
                humanScore ++;
                roundCounter ++;
                }
            }
            System.out.printf("\nScore: human %d, computer %d", humanScore, computerScore);


            System.out.println("\nDo you wish to continue playing? (y/n)?");
            String playAgain =sc.nextLine();
            if (!playAgain.equals("y")){
                System.out.println("Bye bye :)");
                break;
            }}
    }
            
        
    

    



    /**
     * Reads input from console with given prompt
    * @param prompt
    * @return string input answer from user
    */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
        
    
    
}



